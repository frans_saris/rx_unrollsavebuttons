<?php
/*
 * This file is part of the rx_unrollsavebuttons extension.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 3
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace Reelworx\RxUnrollsavebuttons\Xclass;

use TYPO3\CMS\Backend\Template\Components\Buttons\InputButton;
use TYPO3\CMS\Backend\Template\Components\Buttons\SplitButton;
use TYPO3\CMS\Core\Service\DependencyOrderingService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Xclass SplitButton to change rendering
 */
class UnrolledSplitButton extends SplitButton
{
    /**
     * @var bool
     */
    protected $doUnroll = true;

    /**
     * @var bool
     */
    protected $hideText = false;

    /**
     * Set properties for unroll mode
     *
     * @param string $mode
     * @return void
     */
    public function setMode($mode)
    {
        switch ($mode) {
        case 'disableUnroll':
            $this->doUnroll = false;
            break;
        case 'iconOnly':
            $this->doUnroll = true;
            $this->hideText = true;
            break;
        case 'iconAndText':
            $this->doUnroll = true;
            $this->hideText = false;
            break;
        }
    }

    /**
     * Renders the HTML markup of the button
     *
     * @return string
     */
    public function render()
    {
        $settings = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['rx_unrollsavebuttons']);
        $this->setMode($settings['mode']);
        if ($settings['allowUserSettings'] && isset($this->getUser()->uc['unrollButtonMode'])) {
            $this->setMode($this->getUser()->uc['unrollButtonMode']);
        }
        if (!$this->doUnroll) {
            return parent::render();
        }

        $items = $this->getButton();
        /** @var InputButton[] $buttons */
        $buttons = array_merge([ $items['primary'] ], $items['options']);
        $namedButtons = array();
        foreach ($buttons as $option) {
            if (isset($namedButtons[$option->getName()])) { // collision - should not happen
                $namedButtons[$option->getName() . ' ' . md5($option->getValue())] = $option;
            } else {
                $namedButtons[$option->getName()] = $option;
            }
        }

        $namedButtons = $this->orderButtons($namedButtons);

        $renderedButtons = array();
        foreach ($namedButtons as $option) {
            $attributes = [
                'type' => 'submit',
                'class' => 'btn btn-sm btn-default ' . $option->getClasses(),
                'name' => $option->getName(),
                'value' => $option->getValue(),
                'title' => $option->getTitle()
            ];
            $onClick = $option->getOnClick();
            if ($onClick) {
                $attributes['onclick'] = $onClick;
            }
            $form = $option->getForm();
            if ($form) {
                $attributes['form'] = $form;
            }
            $buttonTitle = $option->getTitle();
            if ($this->hideText) {
                $attributes['title'] = $buttonTitle;
                $buttonTitle = '';
            }
            $optionAttributesString = '';
            foreach ($attributes as $key => $value) {
                $optionAttributesString .= ' ' . htmlspecialchars($key) . '="' . htmlspecialchars($value) . '"';
            }

            $renderedButtons[$option->getName()] = '<button' . $optionAttributesString . '>'
                                                   . $option->getIcon()->render()
                                                   . htmlspecialchars($buttonTitle) . '</button>';
        }
        return '<div class="btn-group">' . implode('', $renderedButtons) . '</div>';
    }

    /**
     * Order buttons by user TSconfig
     *
     * Orders the given array of buttons based on the user TSconfig 'options.txunrollsavebuttons.order',
     * which is expected to contain a list of action names.
     *
     * @param InputButton[] $buttons
     *
     * @return InputButton[]
     */
    protected function orderButtons(array $buttons)
    {
        $dependencyArray = $this->getDependencyArrayFromConfig();
        if (empty($dependencyArray)) {
            return $buttons;
        }
        foreach (array_keys($buttons) as $buttonName) {
            if (!isset($dependencyArray[$buttonName])) {
                $dependencyArray[$buttonName] = [];
            }
        }

        /** @var DependencyOrderingService $dependencyOrderingService */
        $dependencyOrderingService = GeneralUtility::makeInstance(DependencyOrderingService::class);
        $orderedItems = array_reverse($dependencyOrderingService->orderByDependencies($dependencyArray));
        foreach ($orderedItems as $name => $_config) {
            if (isset($buttons[$name])) {
                $option = $buttons[$name];
                unset($buttons[$name]);
                array_unshift($buttons, $option);
            }
        }

        return $buttons;
    }

    /**
     * Build the dependency array from TSconfig
     *
     * @return array
     */
    protected function getDependencyArrayFromConfig()
    {
        $rawTsConfigProperties = $this->getUser()->getTSConfigProp('options.txunrollsavebuttons.order');
        if ($rawTsConfigProperties === null) {
            return [];
        }

        $dependencyArray = [];
        foreach ($rawTsConfigProperties as $key => $value) {
            if (is_array($value)) {
                $buttonName = substr($key,0,-1); // remove trailing dot
                $dependencyArray[$buttonName] = [
                    'before' => [],
                    'after' => []
                ];
                foreach (array_keys($dependencyArray[$buttonName]) as $key) {
                    if (isset($value[$key]) && is_string($value[$key])) {
                        $dependencyArray[$buttonName][$key] = GeneralUtility::trimExplode(',', $value[$key], true);
                    }
                }
            }
        }
        return $dependencyArray;
    }

    /**
     * Get the current backend user
     *
     * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected function getUser()
    {
        return $GLOBALS['BE_USER'];
    }
}
